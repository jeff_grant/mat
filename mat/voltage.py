from mat import odlfile
from datetime import datetime
import sys
import os


def extract_voltage(path, time_format='iso8601'):
    with open(path, 'rb') as fid:
        odl_file = odlfile.load_file(fid)
        time = odl_file.page_start_times
        voltage = odl_file.page_start_voltage()

    directory = os.path.dirname(path)
    basename = os.path.basename(path)
    new_filename = basename[:-4] + '_voltage.txt'

    with open(os.path.join(directory, new_filename), 'w') as fid:
        fid.write('Time, Voltage\n')
        for i in range(len(time)):
            if time_format == 'iso8601':
                time_str = datetime.fromtimestamp(time[i]).strftime('%Y-%m-%dT%H:%M:%S')
            elif time_format == 'legacy':
                time_str = datetime.fromtimestamp(time[i]).strftime('%Y-%m-%d,%H:%M:%S')
            elif time_format == 'posix':
                time_str = time[i]

            fid.write('{},{:.3f}\n'.format(time_str, voltage[i]))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        raise ValueError('You must specify a file')
    current_directory = os.getcwd()
    file = sys.argv[1]

    if os.path.isabs(file):
        abs_path = file
    else:
        abs_path = os.path.join(current_directory, file)

    if not os.path.isfile(abs_path):
        raise ValueError('File specified is not found')

    if not abs_path.endswith('.lid'):
        raise ValueError('File must be of type .lid')

    time_format = 'iso8601'
    if len(sys.argv) == 3:
        time_format = sys.argv[2]
        if time_format not in ['iso8601', 'legacy', 'posix']:
            raise ValueError('Unknown time format: ' + time_format)
    try:
        extract_voltage(abs_path, time_format)
        print('Voltage extracted successfully')
    except:
        print('Error extracting voltage from ', + sys.argv[1])